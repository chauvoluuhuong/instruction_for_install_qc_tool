#QC Tool

## PROJECT SCOPE: 

* GET LOG DEBUG, PARSE that data and SHOW STATUS of running PRORGAM for tester.
    
* Make INTERFACE for CONTROL PRINTER, BURNING FIRMWARE, take it MORE EASY and AUTOMATICALLY

* STORE DATA and EXPORT TO DOCUMENT file  

* STATISTIC AND ANALY DATA from WED SERVER    

* UTILIZE supporter at offic  

##Install application:  

**Firstly, you have to install Python Interpret**  

* Go to project folder->'first requirement' folder and click on file: `python-3.7.0-amd64` ,**And step by step choose options like illustrative images bellow For install**  

    1. Step 1:  
    ![alt Step 1](image/install_p_0.PNG)  
    2. Step 2:  
    ![alt Step 2](image/install_p_1.PNG)  
    3. Step 3:  
    ![alt Step 3](image/install_p_2.PNG)

* **Option:** for window version before WINDOW 10 or Your OS is troubled with COM Port.  
After you plug the device to pc, And press `Connect Device` (on QC-Tool UI) button for connect QC-Tool with your device  
**but** the device's COM Port does't appear, Please extract `CP210x_Universal_Windows_Driver.zip` file in 'first requirement' folder install Driver for USB-UART chip  

##How to use app:  
**Extract log-parser.zip and go to achieved folder, try to find QC-TOOL.bat file**  
![alt](image/start_qc_tool.PNG) **Press on that file for start QC-TOOL app.**  
of couse you can send this icon to destop for make using easer :)   

###Connect Device with QC-tool:  
**Fist off all, you must connect Box device with QC-tool** the device need to be connect with QC-tool via COM port.  
Plug the box-plus device to your PC and press  `Connect Device` button  
![alt connect device](image/connect.PNG) and choose box-plus's COM port for connect with device.  
**tip:** Sometime there are multi COM ports will apear on yours computer. Something like that  
![alt list serial ports](image/list_serial_ports.png)   
So you need to determine what is box-plus device COM port.  
You just un-mount your device and check what COM port will be disappear, what COM port disappear that is box-plus's COM port.  
If device connect sucessful, the status `Connect Device` button will be change to like that  
![alt connect successful](image/connect_successul.png)


###Read log debug.
* Log debug will be show on terminal. **You can get log debug by click on button Get Box Log.** ![alt](image/get_box_log.PNG)
* Some properties device, like as `is_wifi_connect`, `firmware version`,`Mac address`... will be show on UI.

###Burn or erase firmware.
* Press those buttons for burn or erase firmware to box-plus.  
![alt burn or erase firmware button](image/burn_or_erase_firmware.png)
* **Some time burning firmware burn will not successful, let's erase firmware first**

###Setup for burn production version into efuse:
* Press on this button for setup production version, that will be burn on efuse.  
![alt setup for burn efuse button](image/setup_for_burn_efuse.png)  
**Caution: those efuses will be burn alog with burn firmware when you press `Burn firmware button`.** 

###Read production number on efuse:  
Press on ![alt](image/read_efuse.PNG) button for read production number.  

##How report bugs:
Plase directly connect with me via email: <huong@hikami.digital.com>  
**For developer:** Please create issue on this repository to discuss with me.